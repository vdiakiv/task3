const fs = require("fs");
const user = require("../database/models/user");

module.exports = file => {
  fs.readFile(file, (err, data) => {
    if (err) {
      // if (err.code === "ENOENT") {
      //   console.log("No such file!");
      // }
      console.log("Error opening file!");
    } else {
      let users = JSON.parse(data);
      user.find({}, (err, data) => {
        if (data.length === 0) {
          user.insertMany(users, err => {
            console.log(err);
          });
        }
      });
    }
  });
};
