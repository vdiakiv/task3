const { saveData } = require("../repositories/user.repository");

const getUser = user => {
  if (user) {
    return user.name;
  } else {
    return null;
  }
};

const saveUser = user => {
  if (user) {
    return saveData(user);
  } else {
    return null;
  }
};

module.exports = {
  getUser,
  saveUser
};
