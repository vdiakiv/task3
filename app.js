require("dotenv").config();

const express = require("express");
const cookieParser = require("cookie-parser");
const logger = require("morgan");

const mongoose = require("mongoose");
const db_uri = process.env.DB_URI || "mongodb://localhost/bsa-express-project";
mongoose.connect(db_uri, { useNewUrlParser: true });
const seedDatabase = require("./services/seed.service");
const path = require("path");
const pathToFile = path.resolve(__dirname, ".", "userlist.json");
seedDatabase(pathToFile);

const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");

const app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use("/", indexRouter);
app.use("/users", usersRouter);

app.use((req, res) => {
  res.status(404).send({ url: req.url + " not found" });
});

module.exports = app;
