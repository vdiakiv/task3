const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    _id: String,
    name: String,
    health: Number,
    attack: Number,
    defense: Number,
    source: String
  },
  { toObject: { versionKey: false } }
);

const user = mongoose.model("user", userSchema);

module.exports = user;
