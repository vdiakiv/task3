var express = require("express");
var router = express.Router();

const { saveUser } = require("../services/user.service");
const { isAuthorized } = require("../middlewares/auth.middleware");

const user = require("../database/models/user");

router.get("/", (req, res, next) => {
  user.find({}, (err, users) => {
    if (err) res.send(err);

    res.json(users);
  });
});

router.get("/:id", (req, res, next) => {
  user.findById(req.params.id, (err, user) => {
    if (err) res.send(err);

    res.json(user);
  });
});

router.post("/", isAuthorized, (req, res, next) => {
  /*const result = saveUser(req.body);
  //console.log(result);
  if (result) {
    res.json(req.body);
  } else {
    res.status(400).send(`Some error`);
  }*/

  user.create(req.body, (error, user) => {
    if (error) {
      res.status(400).send(`Some error`);
    }

    res.json(user);
  });
});

router.put("/:id", (req, res, next) => {
  user.findOneAndUpdate(
    { _id: req.params.id },
    req.body,
    { new: true },
    (err, user) => {
      if (err) res.status(400).send(`Some error`);

      res.json(user);
    }
  );
});

router.delete("/:id", (req, res, next) => {
  user.deleteOne(
    {
      _id: req.params.id
    },
    (err, user) => {
      if (err) res.send(err);

      res.json({ message: "User was deleted" });
    }
  );
});

module.exports = router;
